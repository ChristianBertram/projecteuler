public class Euler3 {
	public static void main(String[] args) {
		long number = Long.parseLong("600851475143");
		for (long i = 2; i > 1; i++) {
			if (number % i == 0) {
				if (isPrime(number/i)) {
					System.out.println(number/i);
					break;
				}
			}
		}
	}
	
	private static boolean isPrime(long n) {
		for (int i =  2; i<Math.sqrt(n); i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}
}