public class Euler2 {
	public static void main(String[] args) {
		int last = 1;
		int curr = 2;
		int sum = 0;
		while (curr < 4000000) {
			if (curr % 2 == 0) {
				sum += curr;
			}
			int temp = curr;
			curr += last;
			last = temp;
		}
		
		System.out.println(sum);
	}
}