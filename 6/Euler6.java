public class Euler6 {
	public static void main(String[] args) {
		long n = Integer.parseInt(args[0]);
		long sum = 0;
		for (long i = 0; i <= n; i++) {
			sum += i*i;
		}
		long square = (n*n+n)*(n*n+n)/4;
		System.out.println(square - sum);
	}
}