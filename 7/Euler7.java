import java.util.ArrayList;

public class Euler7 {
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]); //Find prime number n
		
		ArrayList<Integer> primes = new ArrayList<Integer>();
		primes.add(2);
		int number = 3;
		while (primes.size() < n) {
			boolean prime = true;
			for (int i = 0; i < primes.size(); i++) {
				if (number % primes.get(i) == 0) {
					prime = false;
					break;
				}
			}
			if (prime) {
				primes.add(number);
				System.out.println(number);
			}
			number += 2;
		}
		System.out.println(primes.get(n-1));
	}
}