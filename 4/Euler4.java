public class Euler4 {
	public static void main(String[] args) {
		int start = Integer.parseInt(args[0]); // 900
		int end = Integer.parseInt(args[1]); // 999
		
		for (int i = end; i >= start; i--) {
			for (int j = end; j >= start; j--) {
				String[] product = Integer.toString(i*j).split("");
				boolean palindrome = true;
				for (int x = 0; x < product.length/2; x++) {
					if (Integer.parseInt(product[x]) != Integer.parseInt(product[product.length -1 -x])) {
						palindrome = false;
					}
				}
				if (palindrome) {
					System.out.println(i + " * " + j + " = " + i*j);
					return;
				}
			}
		}
	}
}