public class Euler5 {
	public static void main(String[] args) {
		int top = Integer.parseInt(args[0]);
		
		for (int i = 20; i <= top; i++) {
			boolean evenlyDivisible = true;
			for (int j = 2; j <= 20; j++) {
				if (i % j != 0) {
					evenlyDivisible = false;
					break;
				}
			}
			if (evenlyDivisible) {
				System.out.println(i);
				break;
			}
		}
	}
}